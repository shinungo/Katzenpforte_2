package ch.shinungo.android.katzenpforte_2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Overview extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overview);

    }

    public void gotoSeeCats(View view) {
        Intent intent = new Intent(this, ViewCats.class);
        startActivity(intent);
    }

    public void gotoSettings(View view) {
        Intent intent = new Intent(this, SettingsActivities.class);
        startActivity(intent);
    }


    public void finishActivities(View view) {
        finish();
    }
}
