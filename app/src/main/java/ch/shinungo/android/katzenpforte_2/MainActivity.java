package ch.shinungo.android.katzenpforte_2;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    public static final String EXTRA_MESSAGE = "DIES IST DER ZU BEGINN GESPEICHERTE TEXT";

    Button loginButton, cancelButton;
    EditText usernameField, passwordField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        loginButton = (Button) findViewById(R.id.LoginButton);
        cancelButton = (Button) findViewById(R.id.CancelButton);
        usernameField = (EditText) findViewById(R.id.usernameField);
        passwordField = (EditText) findViewById(R.id.passwordField);


//        loginButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(usernameField.getText().toString().equals("ff") &&
//                        passwordField.getText().toString().equals("ff")) {
//                    Toast.makeText(getApplicationContext(), "Jetzt bis du eingeloggt", Toast.LENGTH_SHORT).show();
//
//                }else{
//
//                    Toast.makeText(getApplicationContext(), "Wrong Credentials",Toast.LENGTH_SHORT).show();
//
//
//                    // Das müsste auf ein separates Field geändert werden. 14.36, 11.07.2019
//                    usernameField.setVisibility(View.VISIBLE);
//                    usernameField.setBackgroundColor(Color.RED);
//                    counter--;
//                    usernameField.setText(Integer.toString(counter));
//
//                    if (counter == 0) {
//                        loginButton.setEnabled(false);
//                    }
//                }
//            }
//        });




    }

    public void finishActivities(View view) {
        finish();
    }

    public void gotoOververview(View vie) {
        Intent intent = new Intent(this, Overview.class);
        if (usernameField.getText().toString().equals("ff") &&
                passwordField.getText().toString().equals("ff")) {
            Toast.makeText(getApplicationContext(), "Jetzt bis du eingeloggt", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Wrong Credentials", Toast.LENGTH_SHORT).show();
        }

        startActivity(intent);

    }


    public void sendMessage(View view) {
        Intent intent = new Intent(this, SettingsActivities.class);
        EditText editText = (EditText) findViewById(R.id.editText);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

}